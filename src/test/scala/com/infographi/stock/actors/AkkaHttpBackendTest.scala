package com.infographi.stock.actors

import scala.concurrent.Future
import com.softwaremill.sttp._
import com.softwaremill.sttp.akkahttp._
import com.softwaremill.sttp.json4s._

import scala.concurrent.ExecutionContext.Implicits.global

object AkkaHttpBackendTest extends App {
  val request = sttp
    .get(uri"https://httpbin.org/get")
    .response(asJson[HttpBinResponse])

  implicit val backend = AkkaHttpBackend()
  try {
    val response: Future[Response[HttpBinResponse]] = request.send()
    for {
      r <- response
    } {
      println(s"Got response code: ${r.code}")
      println(r.body.right.get.body)
      backend.close()
    }
  } catch {
    case e: Exception => e.printStackTrace()
  }
}
case class HttpBinResponse(origin: String, headers: Map[String, String], body: String)