package com.infographi.util

import com.infographi.stock.analytics.StockData
import com.typesafe.config.ConfigFactory
import javax.jms.{Message, MessageListener, ObjectMessage, Session}
import org.apache.activemq.ActiveMQConnectionFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ActiveMQhandlerSpec extends WordSpecLike with Matchers with BeforeAndAfterAll{
  val config = ConfigFactory.load
  val activeMQUrl = config.getString("stocks.mq.url")
  val connectionFactory = new ActiveMQConnectionFactory(activeMQUrl)
  val connection = connectionFactory.createConnection
  val session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
  val queue  = session.createQueue(config.getString("stocks.mq.queue"))
  val consumer = session.createConsumer(queue)

  "An Active MQ Handler" must {
    "Be able to send a message" in {
      val stockData = StockData("TEST", 1.0, 1.0, 1.0, 1.0, 1, "TEST")
      ActiveMQHandler.sendStockDataToQueue(Seq(stockData))
      val listener = new MessageListener {
        def onMessage(message: Message): Unit = {
          message match {
            case obj: ObjectMessage => println(obj)
          }
        }
      }
      consumer.setMessageListener(listener)
      println("Hello")
    }
  }
}
