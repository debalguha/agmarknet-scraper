package com.infographi.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.github.nscala_time.time.Imports.DateTimeFormat
import com.infographi.stock.analytics.StockData
import com.typesafe.config.ConfigFactory
import javax.jms.{DeliveryMode, Session}
import org.apache.activemq.ActiveMQConnectionFactory
import org.apache.commons.lang3.RandomUtils

object ActiveMQHandler {
  val config = ConfigFactory.load
  val activeMQUrl = config.getString("stocks.mq.url")
  val connectionFactory = new ActiveMQConnectionFactory(activeMQUrl)
  val connection = connectionFactory.createConnection
  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd")
  val mapper = new ObjectMapper() with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)

  def toJson(stockData: StockData): String = {
    mapper.writeValueAsString(stockData)
  }

  def sendStockDataToQueue(stockDataSeq: Seq[StockData]): Unit = {
    val session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
    val sendQueue = session.createQueue(config.getString("stocks.mq.queue"))
    val replyQueue = session.createQueue("stocks.mq.reply")

    val producer = session.createProducer(sendQueue)
    producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT)
    stockDataSeq.foreach(stockData => {
      val objMessage = session.createTextMessage(toJson(stockData))
      objMessage.setJMSReplyTo(replyQueue)
      objMessage.setJMSCorrelationID(RandomUtils.nextInt().toString)
      producer.send(objMessage)
    })
  }
}
