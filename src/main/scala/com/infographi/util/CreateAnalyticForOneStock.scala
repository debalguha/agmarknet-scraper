package com.infographi.util

import java.io.File
import java.util.Date

import org.apache.commons.beanutils.{BeanUtils, PropertyUtils}
import org.apache.commons.io.{FileUtils, FilenameUtils}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import purecsv.unsafe.CSVReader

import scala.reflect.runtime.universe._
import scala.collection.JavaConversions._
import scala.collection.mutable

object CreateAnalyticForOneStock extends App {
  val format = DateTimeFormat.forPattern("yyyy-MM-dd")
  val file = new File("C:\\temp\\d_us_txt\\data\\daily\\us\\nyse stocks\\1\\a.us.txt")
  val stockResults = FileUtils.listFiles(new File("C:\\temp\\d_us_txt\\data\\daily\\us\\nyse stocks\\1"), Array("txt"), true).map(processStock(_))
  val refinedStockResult = stockResults.filter(stockResult => stockResult.volume > 0).toList

  def toFile(dataPoints: List[ResultDataPoint], changeMonth: String) = {
    val fileToWrite = new File(new File("output_data"), s"$changeMonth.txt")
    if(!fileToWrite.exists)
      fileToWrite.createNewFile
    FileUtils.writeLines(fileToWrite, mutable.ArrayBuffer("Symbol\tStart Value\tEnd value\tChange Percentage\tVolume") ++ dataPoints.map(dataPoint => s"${dataPoint.symbol}\t${dataPoint.startValue}\t${dataPoint.endValue}\t${dataPoint.changePercent}\t${dataPoint.volume}"))
  }

  toFile(reportChange(refinedStockResult, "change1Month"), "One_Month_Change")
  toFile(reportChange(refinedStockResult, "change3Months"), "Three_Month_Change")
  toFile(reportChange(refinedStockResult, "change6Months"), "Six_Month_Change")
  //oneMonthChangeReport.foreach(aTuple => println(s"${aTuple._1}, ${aTuple._2.startValue}, ${aTuple._2.endValue}, ${aTuple._2.changePercent}"))
  //val threeMonthChangeReport = reportChange(refinedStockResult, "change3Months")
  //val sixMonthChangeReport = reportChange(refinedStockResult, "change6Months")


  def reportChange(refinedStockResult: List[StockResult], propertyName: String):List[ResultDataPoint] = {
    refinedStockResult.map(stockResult => getFieldValue(stockResult, propertyName))
  }
  def getFieldValue(instance: StockResult, fieldName: String): ResultDataPoint = {
    val typeMirror = runtimeMirror(instance.getClass.getClassLoader)
    val instanceMirror = typeMirror.reflect(instance)
    val fieldMirror = typeOf[StockResult].decl(TermName(fieldName)).asTerm.accessed.asTerm
    instanceMirror.reflectField(fieldMirror).get.asInstanceOf[ResultDataPoint]
  }
  def processStock(file: File): StockResult = {
    val allItems = CSVReader[DataItemRaw].readCSVFromFile(file, skipHeader = true, delimiter = ',').map(dataItem => convertFromRaw(dataItem))
    val indexedByYearAndMonth = allItems.toStream.groupBy(item =>{
      val dateTime = new DateTime(item.date)
      s"${dateTime.getYear}-${dateTime.getMonthOfYear}"
    }).seq
    try {
      val (startDate1M, endDate1M) = getStartAndEndOfDateFromNow(1)
      val oneMonthChange = calculatePercentChange(file.getName, startDate1M, endDate1M, indexedByYearAndMonth)
      val (startDate3M, endDate3M) = getStartAndEndOfDateFromNow(3)
      val threeMonthChange = calculatePercentChange(file.getName, startDate3M, endDate3M, indexedByYearAndMonth)
      val (startDate6M, endDate6M) = getStartAndEndOfDateFromNow(6)
      val sixMonthChange = calculatePercentChange(file.getName, startDate6M, endDate6M, indexedByYearAndMonth)
      StockResult(FilenameUtils.getBaseName(file.getName), indexedByYearAndMonth.get(endDate1M).get.seq.last.volume, oneMonthChange, threeMonthChange, sixMonthChange)
    } catch {
      case e: Exception => {
        val dummyResultDataPoints = ResultDataPoint(file.getName, 0d, 0d, 0d, 0L)
        StockResult(FilenameUtils.getBaseName(file.getName), 0L, dummyResultDataPoints, dummyResultDataPoints, dummyResultDataPoints)
      }
    }
  }

  def calculatePercentChange(stockSymbol: String, startDate: String, endDate: String, indexedByYearAndMonth: Map[String, Stream[DataItem]]): ResultDataPoint = {
    val startDataItem = indexedByYearAndMonth.get(startDate).get.sorted.seq(0)
    val endDataItem = indexedByYearAndMonth.get(endDate).get.sorted.seq(0)
    val changePercent = (scala.math.abs(endDataItem.close - startDataItem.open) * 100f)/startDataItem.open
    ResultDataPoint(stockSymbol, startDataItem.open, endDataItem.close, changePercent, endDataItem.volume)
  }

  def getStartAndEndOfDateFromNow(monthBack: Int): (String, String) = {
    val startDate = new DateTime().minusMonths(monthBack+3).dayOfMonth.withMinimumValue
    val endDate = new DateTime().minusMonths(3).dayOfMonth().withMaximumValue
    (s"${startDate.getYear}-${startDate.getMonthOfYear}", s"${endDate.getYear}-${endDate.getMonthOfYear}")
  }

  def convertFromRaw(dataItem: DataItemRaw): DataItem = {
    DataItem(format.parseLocalDate(dataItem.date).toDate, dataItem.open, dataItem.high, dataItem.low, dataItem.close, dataItem.volume)
  }
}

case class DataItemRaw(date: String, open: Double, high: Double, low: Double, close: Double, volume: Long, openInterval: Int)
case class DataItem(date: Date, open: Double, high: Double, low: Double, close: Double, volume: Long) extends Ordered[DataItem] {
  def compare(that: DataItem): Int = this.date compareTo(that.date)
}

case class StockItem(symbol: String, dataItems: List[DataItem])
case class ResultDataPoint(symbol: String, startValue: Double, endValue: Double, changePercent: Double, volume: Long)
case class StockResult(symbol: String, volume: Long, change1Month: ResultDataPoint, change3Months: ResultDataPoint, change6Months: ResultDataPoint)
