package com.infographi

object PropertyKeys {
  val stocksFile = "stocks.file"
  val apiEndPt = "stocks.endpt"
}
