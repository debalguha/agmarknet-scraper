package com.infographi.agmarknet.scraper

import java.net.URL

import akka.actor.{Actor, Props, _}
import akka.pattern.{ask, pipe}
import akka.util.Timeout

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * @author Foat Akhmadeev
  *         17/01/16
  */
class SiteCrawler(supervisor: ActorRef, indexer: ActorRef) extends Actor {
  val process = "Process next url"

  val scraper = context actorOf Props(new Scraper(indexer))
  implicit val timeout = Timeout(3 seconds)
  val tick =
    context.system.scheduler.schedule(0 millis, 1000 millis, self, process)
  val toProcess = mutable.Queue[URL]()
  val stageTwoProcess = mutable.Queue[URL]()
  def receive: Receive = {
    case Scrap(url) => {
      // wait some time, so we will not spam a website
      println(s"waiting... $url")
      toProcess.enqueue(url)
    } case `process` => {
      if(toProcess nonEmpty){
        toProcess.dequeue() match {
          case url =>
            println(s"site scraping... $url")
            (scraper ? Scrap(url)).mapTo[ScrapFinished]
              .recoverWith ({ case e => Future {
                ScrapFailure(url, e)
              }
              }).pipeTo(self)

          case _ =>
        }
      }
    } case ScrapFinished(url) => {
      //println(s"Scrapping finished $url")
    }
  }
}