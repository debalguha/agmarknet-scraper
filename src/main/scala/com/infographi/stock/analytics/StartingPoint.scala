package com.infographi.stock.analytics

import java.net.URL

import akka.actor.{ActorSystem, PoisonPill, Props}
import com.infographi.stock.actors.Supervisor
import com.typesafe.config.ConfigFactory

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * @author Foat Akhmadeev
  *         17/01/16
  */
object StartingPoint extends App {
  val config = ConfigFactory.load
  val system = ActorSystem()
  val supervisor = system.actorOf(Props(classOf[Supervisor], system, config), "supervisor")

  supervisor ! Start("START")

  /*Await.result(system.whenTerminated, 10 minutes)

  supervisor ! PoisonPill*/

  def terminateActorSystem(): Unit = {
    println("terminating system....")
    system.terminate
    println("Done....Packed up!!")
  }

}
