package com.infographi.stock.analytics

import java.net.URL
import java.util.Date

case class Stock(symbol: String)
case class Start(str: String)
case class Crawl(stock: Stock)
case class Scrap(url: URL, stock: Stock)
case class ScrapFinish(stockData: Seq[StockData])
case class ScrapFailed(stock: Stock, reason: Throwable)
case class Persist(stockData: Seq[StockData])
case class PersistFinish(stock: Stock)
case class PersistFailed(stockData: Seq[StockData], reason: Throwable)
case class Finish(stock: Stock)

case class StockData(symbol: String, open: Double, high: Double, low: Double, close: Double, volume: Long, date: String)