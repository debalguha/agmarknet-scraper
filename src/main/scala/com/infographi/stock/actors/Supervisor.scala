package com.infographi.stock.actors

import java.io.File

import akka.actor.{Actor, ActorRef, ActorSystem, PoisonPill, Props}
import com.infographi.PropertyKeys
import com.infographi.stock.analytics._
import com.typesafe.config.Config
import org.apache.commons.io.FileUtils
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class Supervisor (system: ActorSystem, config: Config) extends Actor {
  val LOG = LoggerFactory.getLogger(classOf[Supervisor])
  val ERR_CHECK = "Check for Failure"
  val stockList = FileUtils.readLines(new File(config.getString(PropertyKeys.stocksFile)), "UTF-8").toSeq
  val finishedStocks = mutable.ArrayBuffer[Stock]()
  val failedStocks = mutable.ArrayBuffer[Stock]()
  val persistActor: ActorRef = system.actorOf(Props(classOf[StockPersisterActor], config, self))
  val failureQueue = mutable.Queue[Stock]()
  val failureCountMap = mutable.HashMap[Stock, Int]()
  val retryMax = 5
  val scraperActor = system.actorOf(Props(classOf[ScraperActor], config, self), "scraper")
  val crawlActor = system.actorOf(Props(classOf[StockCrawlActor], config, scraperActor), "crawler")

  val tick = context.system.scheduler.schedule(10 seconds, 30 seconds, self, ERR_CHECK)


  def handleFailure(stock: Stock, e: Throwable): Unit = {
    LOG.info(s"Stock $stock failed for ${e.getMessage}. Adding to Failure queue.")
    val retryCount = failureCountMap.get(stock).getOrElse(0)
    if(retryCount < retryMax){
      failureQueue.enqueue(stock)
      failureCountMap.update(stock, (retryCount+1))
    }else{
      LOG.info(s"Retried $retryMax times for $stock. Failing it for good!")
      failedStocks += stock
    }
  }


  def receive: Receive = {
    case Start(str) => {
      stockList.foreach(stock => crawlActor ! Crawl(Stock(stock)))
    } case ScrapFailed(stock, e) => {
      handleFailure(stock, e)
    } case ERR_CHECK => {
      LOG.info("Scheduler wake up")
      if(failureQueue nonEmpty){
        while(failureQueue nonEmpty){
          crawlActor ! Crawl(failureQueue.dequeue())
        }
      } else {
        if(finishedStocks.size + failedStocks.size == stockList.size){
          self ! PoisonPill
        }
      }
    } case ScrapFinish(stockData) => {
      LOG.info("Finished scraping, sending to persister!")
      persistActor ! Persist(stockData)
    } case PersistFailed(stockData, e) => {
      LOG.info(s"Failed to persist ${stockData(0).symbol}")
    }case PersistFinish(stock) => {
      LOG.info(s"Finished scraping $stock.")
      finishedStocks += stock
    }
  }

  override def postStop(): Unit = {
    super.postStop()
    LOG.info("!!!!!!!!!!!!!!!!!!!Received Stop request.........Printing Stats!!!!!!!!!!!!!!!!!!!!!!!")
    LOG.info(s"Total ${stockList.size} stocks processed.")
    LOG.info(s"Total ${finishedStocks.size} stocks finished properly.")
    LOG.info(s"Total ${failedStocks.size} stocks failed. => ${failedStocks.mkString(",")}")
    Future{StartingPoint.terminateActorSystem()}
  }

}
