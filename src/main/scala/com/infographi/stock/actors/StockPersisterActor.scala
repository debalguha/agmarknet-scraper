package com.infographi.stock.actors

import akka.actor.{Actor, ActorRef}
import akka.pattern.pipe
import akka.util.Timeout
import com.infographi.stock.analytics._
import com.infographi.util.ActiveMQHandler
import com.typesafe.config.Config

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionException, Future}
import scala.concurrent.duration._
import scala.util.Try

class StockPersisterActor(config: Config, supervisor: ActorRef) extends Actor{
  val process = "Persist next stock"
  implicit val timeout = Timeout(3 seconds)
  //val toProcess = new java.util.concurrent.LinkedBlockingQueue[Seq[StockData]]()
  val toProcess = mutable.Queue[Seq[StockData]]()
  val tick = context.system.scheduler.schedule(0 millis, 1000 millis, self, process)
  override def receive: Receive = {
    case Persist(stockData) => {
      toProcess.enqueue(stockData)
    } case `process` => {
      if(toProcess nonEmpty){
        val stockData = toProcess.dequeue();
        Future{sendToKafka(stockData)}.mapTo[PersistFinish].recoverWith({
          case e => Future{PersistFailed(stockData, e)}
        }).pipeTo(supervisor)
      }
    }
  }
  def sendToKafka(stockData: Seq[StockData]): PersistFinish = {
    val trySend = Try({ActiveMQHandler.sendStockDataToQueue(stockData)})
    if(trySend isFailure){
      throw trySend.failed.get
    } else {
      PersistFinish(Stock(stockData(0).symbol))
    }
  }
}
