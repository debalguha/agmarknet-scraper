package com.infographi.stock.actors

import akka.actor.Actor
import com.infographi.stock.analytics.{Finish, Scrap, ScrapFinish}
import com.softwaremill.sttp._
import com.softwaremill.sttp.akkahttp.AkkaHttpBackend
import com.softwaremill.sttp.json4s._
import com.typesafe.config.Config

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class HttpBinResponse(origin: String, headers: Map[String, Any])

class ScraperActorAkkaHttp(config: Config) extends Actor{
  implicit val backend = AkkaHttpBackend()

  override def receive: Receive = {
    case Scrap(url, stock) =>
      println(s"scraping $url")
      val request = sttp.get(Uri(url.toURI)).response(asJson[HttpBinResponse])
      val response: Future[Response[HttpBinResponse]] = request.send()
      for {r <- response} {
        println(s"Got response code: ${r.body}")
        println(r.body.right.getOrElse(r.body.left.get))
        backend.close()
        sender ! Finish(stock)
      }
  }
}
