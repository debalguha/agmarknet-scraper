package com.infographi.stock.actors

import akka.actor.{Actor, ActorRef}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.infographi.stock.analytics._
import com.softwaremill.sttp._
import com.typesafe.config.Config
import com.github.nscala_time.time.Imports._


class ScraperActor(config: Config, supervisor: ActorRef) extends Actor{
  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd")
  override def receive: Receive = {
    case Scrap(url, stock) =>
      println(s"scraping $url")
      //implicit val backend = AkkaHttpBackend()
      implicit val backend = HttpURLConnectionBackend()
      val request = sttp.get(Uri(url.toURI))
      val response = request.send()
      val stockDataSeq = convertMapToStockData(convertBodyToMap(response.body.right.get), stock)
      supervisor ! ScrapFinish(stockDataSeq)
  }
  def convertBodyToMap(body: String): Map[String, Map[String, String]] = {
    val mapper = new ObjectMapper() with ScalaObjectMapper
    mapper.registerModule(DefaultScalaModule)
    val myMap = mapper.readValue[Map[String, Any]](body)
    myMap.get("Monthly Time Series").get.asInstanceOf[Map[String, Map[String, String]]]
  }

  def convertMapToStockData(dataMap: Map[String, Map[String, String]], stock: Stock): Seq[StockData] = {
    dataMap.map(aTuple => convertATupleToStockData(aTuple, stock)).toSeq
  }

  def convertATupleToStockData(aTuple: (String, Map[String, String]), stock: Stock): StockData = {
    val date = aTuple._1//formatter.parseLocalDate(aTuple._1).toDate
    val open = aTuple._2.get("1. open").get.toDouble
    val high = aTuple._2.get("2. high").get.toDouble
    val low = aTuple._2.get("3. low").get.toDouble
    val close = aTuple._2.get("4. close").get.toDouble
    val volume = aTuple._2.get("5. volume").get.toLong
    StockData(stock.symbol, open, high, low, close, volume, date)
  }
}
