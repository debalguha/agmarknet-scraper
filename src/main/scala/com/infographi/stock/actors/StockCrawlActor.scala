package com.infographi.stock.actors

import java.net.URL

import akka.actor.{Actor, ActorRef}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import com.infographi.PropertyKeys
import com.infographi.stock.analytics._
import com.typesafe.config.Config

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class StockCrawlActor(config: Config, scraper: ActorRef) extends Actor{
  val process = "Process next url"
  implicit val timeout = Timeout(3 seconds)
  val toProcess = mutable.Queue[Stock]()
  val tick = context.system.scheduler.schedule(1000 millis, 5 seconds, self, process)

  val apiTemplate = config.getString(PropertyKeys.apiEndPt)

  override def receive: Receive = {
    case Crawl(stock) => {
      toProcess.enqueue(stock)
    } case `process` => {
      if(toProcess nonEmpty){
        val stock = toProcess.dequeue()
        val apiEndPt = s"$apiTemplate&symbol=${stock.symbol}"
        (scraper ? Scrap(new URL(apiEndPt), stock)).mapTo[ScrapFinish].recoverWith({
          case e => Future{ScrapFailed(stock, e)}
        }).pipeTo(sender)
      }
    }
  }
}
