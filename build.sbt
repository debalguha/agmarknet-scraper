name := "agmarknet-scraper"

version := "0.1"

scalaVersion := "2.11.7"

resolvers += Resolver.sonatypeRepo("releases")

libraryDependencies ++= {
  val akkaV       = "2.5.13"
  Seq(
    "com.typesafe.akka" %% "akka-actor"                           % akkaV,
    //"org.jsoup"         % "jsoup"                                 % "1.8+",
    "commons-validator" % "commons-validator"                     % "1.5+",
    "com.softwaremill.sttp" %% "core"                           % "1.2.0-RC6",
    "com.softwaremill.sttp" %% "akka-http-backend"                % "1.2.0-RC6",
    "com.softwaremill.sttp" %% "json4s"                           % "1.2.0-RC6",
    "com.typesafe.akka" %% "akka-stream"                          % "2.5.11"
  )
}
libraryDependencies += "net.ruippeixotog" %% "scala-scraper" % "2.1.0"
// https://mvnrepository.com/artifact/commons-io/commons-io
libraryDependencies += "commons-io" % "commons-io" % "2.6"
// https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-scala
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.5"
libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "2.20.0"
// https://mvnrepository.com/artifact/org.apache.activemq/activemq-client
libraryDependencies += "org.apache.activemq" % "activemq-client" % "5.15.4"
libraryDependencies += "com.github.melrief" %% "purecsv" % "0.1.1"

// https://mvnrepository.com/artifact/org.json4s/json4s-jackson
//  libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.0-M4"
// https://mvnrepository.com/artifact/com.softwaremill.sttp/akka-http-backend
//libraryDependencies += "com.softwaremill.sttp" %% "akka-http-backend" % "1.2.0-RC6"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.13" % Test
// https://mvnrepository.com/artifact/org.scalatest/scalatest
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
libraryDependencies ++= Seq("org.slf4j" % "slf4j-api" % "1.7.7", "org.slf4j" % "slf4j-log4j12" % "1.7.7")


        
